# Adapted from https://github.com/muccc/flipdots/blob/master/scripts/clock.py
import display
from utime import sleep
import utime
import math
import leds

class Time:
    def __init__(self, start = 0):
        self.time = start
        self.wait_time = 0.95

    def tick(self):
        sleep(self.wait_time)
        self.time += 1

    @property
    def second(self):
        return self.time % 60

    @property
    def minute(self):
        return (self.time / 60) % 60

    @property
    def hour(self):
        return (self.time / 3600) % 24

class Clock:
    def __init__(self, sizex = 80, sizey = 80, radius = 38, offsetx = 30,
            hour_hand = True, minute_hand = True, second_hand = True, console_out = False,
            run_once = False, update_interval = 0):
        self.sizex = sizex
        self.sizey = sizey
        self.radius = radius
        self.center = (int(self.sizex/2), int(self.sizey/2))
        self.hour_hand = hour_hand
        self.minute_hand = minute_hand
        self.second_hand = second_hand
        self.console_out = console_out
        self.update_interval = update_interval if update_interval != 0 else (1 if self.second_hand else 30)
        self.run_once = run_once
        self.offsetx = offsetx
        self.time = Time()
        white = (255, 255, 255)
        self.center_col = white
        self.m1_col = white
        self.m5_col = white
        self.hour_hand_col = white
        self.minute_hand_col = white
        self.second_hand_col = white

    def loop(self):
        colored = False
        try:
            with display.open() as disp:
                while True:
                    self.updateClock(disp)
                    if self.run_once:
                        break
        except KeyboardInterrupt:
            for i in range(11):
                leds.set(i, (0, 0, 0))
            return

    def drawImage(self, image):
        with display.open() as d:
            d.clear()
            for x in range(len(image)):
                for y in range(len(image[x])):
                    d.pixel(x + self.offsetx, y, col = (255, 255, 255) if image[x][y] else (0, 0, 0))
            d.update()

    def updateClock(self, disp):
        disp.clear()
        localtime = utime.localtime()

	hour = localtime[3]
	minute = localtime[4]
	seconds = localtime[5]
        disp.print("{:02}:{:02}:{:02}".format(hour, minute, seconds), posx=self.center[0]-20, posy=self.center[1]-10)

        if self.console_out:
            for y in range(self.radius*2):
                line = ""
                for x in range(self.radius*2):
                    line = line + ("." if image[(self.center[1]-self.radius)+y][(self.center[0]-self.radius)+x] else " ")
                print(line)

        disp.update()

    def circlePoint(self, t):
        return (int(round(self.radius*math.cos(t))) + self.center[0], int(round(self.radius*math.sin(t))) + self.center[1])

    def addLine(self, disp, source, aim, length, thickness = 1, col = (255, 255, 255)):
        vector = self.subVector(aim, source)
        vector = self.normVector(vector)
        destination = self.addVector(source, self.multiplyVector(vector, length))

        disp.line(round(source[0]) + self.offsetx, round(source[1]), round(destination[0]) + self.offsetx, round(destination[1]), col=col, size=thickness)

    def normVector(self, v):
        length = math.sqrt(sum([i**2 for i in v]))
        new_v = []
        for i in range(len(v)):
            new_v.append(v[i]/length)
        return tuple(new_v)

    def subVector(self, v1, v2):
        res = []
        for i in range(len(v1)):
            res.append(v1[i]-v2[i])
        return tuple(res)

    def addVector(self, v1, v2):
        res = []
        for i in range(len(v1)):
            res.append(v1[i]+v2[i])
        return tuple(res)

    def multiplyVector(self, v, multiplier):
        return tuple([i*multiplier for i in v])

clock = Clock()
clock.loop()
